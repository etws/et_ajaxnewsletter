<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_AjaxNewsletter
 * @copyright  Copyright (c) 2012 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

class ET_AjaxNewsletter_ReportController extends Mage_Core_Controller_Front_Action
{
    /**
     * New subscription action
     */
    public function showAction()
    {
        $messages = Mage::getSingleton('core/session')->getMessages(true);
        if ($message = $messages->getItemsByType(Mage_Core_Model_Message::SUCCESS)) {
            $result = array("status" => "success", "message" => $this->_getMessageTexts($message));
        } elseif ($message = $messages->getItemsByType(Mage_Core_Model_Message::ERROR)) {
            $result = array("status" => "failed", "message" => $this->_getMessageTexts($message));
        } elseif ($message = $messages->getItemsByType(Mage_Core_Model_Message::WARNING)) {
            $result = array("status" => "failed", "message" => $this->_getMessageTexts($message));
        } else {
            $result = array("status" => "none", "message" => "");
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    protected function _getMessageTexts($messages)
    {
        $texts = array();
        foreach ($messages as $message) {
            $texts[] = $message->getText();
        }
        return implode("<br />", $texts);
    }

}