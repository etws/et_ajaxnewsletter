== Extension links
Permanent link: http://shop.etwebsolutions.com/eng/et-ajax-newsletter.html
Support link: http://support.etwebsolutions.com/projects/et-ajaxnewsletter/roadmap

== Short Description
Модуль позволяет реализовать работу с формой подписки на новости без перезагрузки страницы.


== Version Compatibility
Magento CE:
1.4.x (tested in 1.4.2.0.)
1.5.x (tested in 1.5.0.1.)
1.6.x (tested in 1.6.1.0.)
1.7.x (tested in 1.7.0.2.)

== Installation
* Disable compilation if it is enabled (System -> Tools -> Compilation)
* Disable cache if it is enabled (System -> Cache Management)
* Download the extension or install the extension from Magento Connect
* If you have downloaded it, copy all files from the "install" folder to
  the Magento root folder - where your index.php is
* Log out from the admin panel
* Log in to the admin panel with your login and password
* Set extension's parameters (System -> Configuration -> CUSTOMERS -> Newsletter -> Subscription Options)
* Run the compilation process and enable cache if needed
